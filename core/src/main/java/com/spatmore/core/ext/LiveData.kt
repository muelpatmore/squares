package com.spatmore.core.ext

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.observeNonNull(owner: LifecycleOwner, observer: Observer<T>) {
    observe(owner, Observer { value -> value?.let { observer.onChanged(it) } })
}