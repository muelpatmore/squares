package com.spatmore.core.ext

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

/**
 * Note: personal extension function (https://gist.github.com/daydreamapps/fac27869d58f87774f3d8c57c3f71e99)
 */
fun ViewGroup.inflateChild(@LayoutRes resource: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(resource, this, attachToRoot)