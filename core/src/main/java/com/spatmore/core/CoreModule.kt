package com.spatmore.core

import io.reactivex.schedulers.Schedulers
import org.koin.core.qualifier.named
import org.koin.dsl.module

object CoreModule {

    const val COMPUTATION_SCHEDULER = "scheduler:computation"
    const val DEBOUNCE_SCHEDULER = "scheduler:debounce"
    const val IO_SCHEDULER = "scheduler:io"

    val module = module {

        factory(named(DEBOUNCE_SCHEDULER)) { Schedulers.computation() }
        factory(named(IO_SCHEDULER)) { Schedulers.io() }
        factory(named(COMPUTATION_SCHEDULER)) { Schedulers.computation() }
    }
}
