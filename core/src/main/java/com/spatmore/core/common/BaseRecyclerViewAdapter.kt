package com.spatmore.core.common

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Note: personal helper class (https://gist.github.com/daydreamapps/f5af3a09a828c4a2f01732778b601b4d)
 */
abstract class BaseRecyclerViewAdapter<T> : RecyclerView.Adapter<BaseRecyclerViewAdapter.BaseViewHolder<T>>() {

    private val _models = ArrayList<T>()
    var models: List<T>
        get() = _models
        set(value) {
            _models.clear()
            _models.addAll(value)
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = models.size

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(models[position])
    }

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

        abstract fun bind(model: T)
    }
}