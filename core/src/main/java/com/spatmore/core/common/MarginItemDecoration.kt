package com.spatmore.core.common

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MarginItemDecoration(
        private val spaceHeight: Int,
        private vararg val orientation: Orientation = arrayOf(Orientation.VERTICAL)
) : RecyclerView.ItemDecoration() {

    enum class Orientation { HORIZONTAL, VERTICAL }

    override fun getItemOffsets(
            outRect: Rect, view: View,
            parent: RecyclerView, state: RecyclerView.State
    ) {
        with(outRect) {
            if (Orientation.VERTICAL in orientation) {
                bottom = spaceHeight
                top = if (parent.getChildAdapterPosition(view) == 0) spaceHeight else 0
            }

            if (Orientation.HORIZONTAL in orientation) {
                left = spaceHeight
                right = spaceHeight
            } else {
                left = 0
                right = 0
            }
        }
    }
}