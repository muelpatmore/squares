package com.spatmore.data

import com.spatmore.data.models.local.GithubRepo
import io.reactivex.Observable
import timber.log.Timber

interface IRepository {

    fun get(): Observable<List<GithubRepo>>
    fun getFromRemote(): Observable<List<GithubRepo>>
}

internal class Repository(
    private val service: Service,
    private val Cache: Cache
) : IRepository {

    override fun get(): Observable<List<GithubRepo>> {
        return if (Cache.isCacheStale) {
            Cache.get().filter { it.isNotEmpty() }
                .mergeWith(getFromRemote().filter { it.isNotEmpty() })
        } else {
            Cache.get()
        }
    }

    override fun getFromRemote(): Observable<List<GithubRepo>> {
        return service.getRepos()
            .map { repoDetails -> repoDetails.map { it.githubRepo } }
            .doOnNext { Timber.v("${it.size} values received from api") }
            .flatMap { Cache.updateCache(it).andThen(Observable.just(it)) }
            .onErrorReturn {
                Timber.e(it)
                emptyList()
            }
    }
}