package com.spatmore.data

import androidx.room.Room
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.spatmore.core.CoreModule.COMPUTATION_SCHEDULER
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DataModule {

    val module = module {

        factory<Service> {
            Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Service::class.java)
        }

        single {
            Room.databaseBuilder(get(), Database::class.java, "GithubDB")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }

        factory { get<Database>().getRepoDao() }

        factory { Cache(get(), get(named(COMPUTATION_SCHEDULER)), get()) }

        factory<IRepository> { Repository(get(), get()) }

        factory { DataPreferences(get()) }
    }
}