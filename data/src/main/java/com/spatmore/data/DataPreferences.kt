package com.spatmore.data

import android.content.SharedPreferences
import com.spatmore.core.common.SharedPreferencesHelper

internal class DataPreferences(sharedPreferences: SharedPreferences): SharedPreferencesHelper(sharedPreferences) {

    var lastCacheTime: Long by preference()
}