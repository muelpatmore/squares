package com.spatmore.data

import com.spatmore.data.daos.GithubRepoDao
import com.spatmore.data.models.local.GithubRepo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import timber.log.Timber
import java.util.concurrent.TimeUnit

internal val cacheStaleDuration = TimeUnit.MINUTES.toMillis(1) // set short for testing

internal class Cache(
    private val githubRepoDao: GithubRepoDao,
    private val computationScheduler: Scheduler,
    private val preferences: DataPreferences
) {

    internal val isCacheStale: Boolean
        get() {
            val cacheAge = System.currentTimeMillis() - preferences.lastCacheTime
            Timber.v("Cache age: $cacheAge")
            return cacheStaleDuration < cacheAge
        }

    internal fun get(): Observable<List<GithubRepo>> {
        return githubRepoDao.get()
            .onErrorReturn {
                Timber.e(it)
                emptyList()
            }
            .doOnNext { Timber.v("${it.size} values loaded from Cache") }
            .subscribeOn(computationScheduler)
    }

    internal fun updateCache(repos: List<GithubRepo>): Completable {
        return Completable.fromCallable { githubRepoDao.insert(repos) }
            .subscribeOn(computationScheduler)
            .observeOn(computationScheduler)
            .doOnComplete {
                preferences.lastCacheTime = System.currentTimeMillis()
                Timber.v("Cache updated with ${repos.size} values")
            }
            .doOnError {
                Timber.e(it)
            }
    }
}