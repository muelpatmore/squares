package com.spatmore.data

import com.spatmore.data.models.server.Details
import io.reactivex.Observable
import retrofit2.http.GET

internal interface Service {

    @GET("orgs/square/repos")
    fun getRepos(): Observable<List<Details>>
}

