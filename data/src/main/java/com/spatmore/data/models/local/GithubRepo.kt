package com.spatmore.data.models.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GithubRepo(
    @PrimaryKey
    val id: Int,
    val name: String,
    val description: String,
    val fullName: String,
    val ownerId: Int,
    val url: String,
    val createdAt: Long,
    val updatedAt: Long
)