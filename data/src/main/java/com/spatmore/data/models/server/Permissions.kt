package com.spatmore.data.models.server

import com.google.gson.annotations.SerializedName

internal data class Permissions(

    @SerializedName("admin")
    val admin: Boolean,

    @SerializedName("pull")
    val pull: Boolean,

    @SerializedName("push")
    val push: Boolean
)