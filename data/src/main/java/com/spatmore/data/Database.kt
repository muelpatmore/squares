package com.spatmore.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.spatmore.data.daos.GithubRepoDao
import com.spatmore.data.models.local.GithubRepo

@Database(version = 2, entities = [GithubRepo::class], exportSchema = false)
internal abstract class Database : RoomDatabase() {

    abstract fun getRepoDao(): GithubRepoDao
}