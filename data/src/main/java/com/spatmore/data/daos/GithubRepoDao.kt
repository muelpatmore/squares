package com.spatmore.data.daos

import androidx.annotation.WorkerThread
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.spatmore.data.models.local.GithubRepo
import io.reactivex.Observable

@WorkerThread
@Dao
internal interface GithubRepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(githubRepo: GithubRepo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(githubRepo: List<GithubRepo>)

    @Query("SELECT * FROM GithubRepo")
    fun get(): Observable<List<GithubRepo>>
}