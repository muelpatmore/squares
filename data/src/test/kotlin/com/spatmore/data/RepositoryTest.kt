package com.spatmore.data

import com.spatmore.data.models.local.GithubRepo
import com.spatmore.data.models.server.Details
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import timber.log.Timber


class RepositoryTest {

    private val service: Service = mockk(relaxed = false)
    private val Cache: Cache = mockk(relaxed = false)
    private val githubRepo: GithubRepo = mockk(relaxed = false)
    private val details: Details = mockk(relaxed = false)

    private val subject: IRepository = Repository(service, Cache)

    @Before
    fun setUp() {
        mockkStatic(Timber::class)

        every { Cache.get() } returns Observable.just(listOf(githubRepo))
        every { Cache.get() } returns Observable.just(listOf(githubRepo))
        every { Cache.updateCache(listOf(githubRepo)) } returns Completable.complete()
        every { Cache.isCacheStale } returns true
        every { details.githubRepo } returns githubRepo
    }

    @Test
    fun `getRepos - cache returns fresh values - returns values`() {
        every { Cache.isCacheStale } returns false
        subject.get().test()
            .assertValue(listOf(githubRepo))

        verify(exactly = 1) {
            Cache.get()
        }
    }

    @Test
    fun `getRepos - cache returns empty - calls api, updates cache`() {
        every { Cache.isCacheStale } returns true
        every { service.getRepos() } returns Observable.just(listOf(details))

        subject.get().test()
            .assertValues(listOf(githubRepo), listOf(githubRepo))

        verify(exactly = 1) {
            Cache.isCacheStale
            Cache.get()
            service.getRepos()
        }
    }

    @Test
    fun `getRepos - cache returns empty, api call fails - get from cache`() {
        val error = RuntimeException()
        every { Cache.get() } returns Observable.empty()
        every { service.getRepos() } returns Observable.error(error)

        subject.get().test()
            .assertNoValues()

        verify(exactly = 1) {
            Cache.isCacheStale
            service.getRepos()
            Timber.e(error)
        }
        verify(exactly = 0) {
            details.githubRepo
            Cache.updateCache(any())
        }
    }
}