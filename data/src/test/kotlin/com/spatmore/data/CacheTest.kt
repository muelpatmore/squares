package com.spatmore.data

import com.google.common.truth.Truth.assertThat
import com.spatmore.data.daos.GithubRepoDao
import com.spatmore.data.models.local.GithubRepo
import io.mockk.*
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import timber.log.Timber

class CacheTest {

    private val githubRepoDao: GithubRepoDao = mockk(relaxed = false)
    private val preferences: DataPreferences = mockk(relaxed = false)
    private val computationScheduler: Scheduler = Schedulers.trampoline()
    private val githubRepo: GithubRepo = mockk()
    private val error = RuntimeException()

    private val subject = Cache(githubRepoDao, computationScheduler, preferences)

    @Before
    fun setUp() {
        clearAllMocks()
        mockkStatic(Timber::class)
    }

    @Test
    fun `isCacheStale - cache stale (0) - returns true`() {
        every { preferences.lastCacheTime } returns 0L

        assertThat(subject.isCacheStale).isTrue()

        verify { preferences.lastCacheTime }
    }

    @Test
    fun `isCacheStale - cache stale (near limit) - returns true`() {
        every { preferences.lastCacheTime } answers { System.currentTimeMillis() - (cacheStaleDuration + 500) }

        assertThat(subject.isCacheStale).isTrue()

        verify { preferences.lastCacheTime }
    }

    @Test
    fun `isCacheStale - cache not stale (near limit) - returns cache contents`() {
        every { preferences.lastCacheTime } answers { System.currentTimeMillis() - (cacheStaleDuration - 500) }

        assertThat(subject.isCacheStale).isFalse()

        verify {
            preferences.lastCacheTime
        }
    }

    @Test
    fun `isCacheStale - cache not stale (new) - returns cache contents`() {
        every { preferences.lastCacheTime } answers { System.currentTimeMillis() - 500 }

        assertThat(subject.isCacheStale).isFalse()

        verify {
            preferences.lastCacheTime
        }
    }

    @Test
    fun `get - cache not stale (new) - returns cache contents`() {
        every { preferences.lastCacheTime } answers { System.currentTimeMillis() - 500 }
        every { githubRepoDao.get() } returns Observable.just(listOf(githubRepo))

        subject.get().test()
            .assertValue(listOf(githubRepo))

        verify {
            githubRepoDao.get()
        }
    }

    @Test
    fun `updateRepoCache - calls githubRepo`() {
        every { githubRepoDao.insert(listOf(githubRepo)) } just Runs

        subject.updateCache(listOf(githubRepo)).test()

        verify { githubRepoDao.insert(listOf(githubRepo)) }
    }

    @Test
    fun `updateRepoCache - githubRepo throws exception - error is logged`() {
        every { githubRepoDao.insert(listOf(githubRepo)) } throws error

        subject.updateCache(listOf(githubRepo)).test()

        verify {
            githubRepoDao.insert(listOf(githubRepo))
            Timber.e(error)
        }
    }
}