package com.spatmore.squares

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.spatmore.data.IRepository
import com.spatmore.data.models.local.GithubRepo
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import timber.log.Timber

class MainViewModelTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    private val repoRepository: IRepository = mockk(relaxed = false)

    private lateinit var subject: MainViewModel

    @Before
    fun setUp() {
        subject = MainViewModel(repoRepository, Schedulers.trampoline())
    }

    @Test
    fun `repoNames - observable returns empty - live data contains no value`() {
        every { repoRepository.get() } returns Observable.empty()

        subject.repoNames.test()
            .assertNoValue()

        verify { repoRepository.get() }
    }

    @Test
    fun `repoNames - observable returns empty list - live data contains empty list`() {
        every { repoRepository.get() } returns Observable.just(emptyList())

        subject.repoNames.test()
            .assertValue(emptyList())

        verify { repoRepository.get() }
    }

    @Test
    fun `repoNames - observable returns list of repos - live data contains list of repos`() {
        val repo = mockk<GithubRepo>()
        every { repoRepository.get() } returns Observable.just(listOf(repo))

        subject.repoNames.test()
            .assertValue(listOf(repo))

        verify { repoRepository.get() }
    }

    @Test
    fun `repoNames - observable returns error - live data contains empty list`() {
        mockkStatic(Timber::class)
        val error = RuntimeException()
        every { repoRepository.get() } returns Observable.error(error)

        subject.repoNames.test()
            .assertValue(emptyList())

        verify {
            repoRepository.get()
            Timber.e(error)
        }
    }
}