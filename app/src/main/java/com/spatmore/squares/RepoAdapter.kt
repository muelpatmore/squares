package com.spatmore.squares

import android.view.View
import android.view.ViewGroup
import com.spatmore.core.common.BaseRecyclerViewAdapter
import com.spatmore.data.models.local.GithubRepo
import com.spatmore.core.ext.inflateChild
import kotlinx.android.synthetic.main.repo_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class RepoAdapter : BaseRecyclerViewAdapter<GithubRepo>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<GithubRepo> {
        return AdapterViewHolder(parent.inflateChild(R.layout.repo_list_item))
    }

    inner class AdapterViewHolder(itemView: View) : BaseRecyclerViewAdapter.BaseViewHolder<GithubRepo>(itemView) {

        private val formatter = SimpleDateFormat("dd MMMM", Locale.getDefault())

        override fun bind(model: GithubRepo) {
            itemView.repoName.text = model.name
            itemView.repoUpdated.text =
                itemView.context?.getString(R.string.last_updated_template, formatter.format(Date(model.updatedAt)))
                    ?: ""
        }
    }
}