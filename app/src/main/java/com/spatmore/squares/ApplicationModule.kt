package com.spatmore.squares

import android.content.Context
import com.spatmore.core.CoreModule.IO_SCHEDULER
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named

object ApplicationModule {

    val module = org.koin.dsl.module {

        viewModel { MainViewModel(get(), get(named(IO_SCHEDULER))) }

        factory { get<Context>().getSharedPreferences("SquareApplication", 0) }
    }
}