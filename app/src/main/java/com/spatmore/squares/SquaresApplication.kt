package com.spatmore.squares

import android.app.Application
import com.spatmore.core.CoreModule
import com.spatmore.data.DataModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class SquaresApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.BUILD_TYPE == "debug") {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidLogger()
            androidContext(this@SquaresApplication)
            modules(
                listOf(
                    ApplicationModule.module,
                    CoreModule.module,
                    DataModule.module
                )
            )
        }
    }
}