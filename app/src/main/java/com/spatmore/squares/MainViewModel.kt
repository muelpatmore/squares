package com.spatmore.squares

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import com.spatmore.data.IRepository
import com.spatmore.data.models.local.GithubRepo
import io.reactivex.BackpressureStrategy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainViewModel(
    private val repository: IRepository,
    private val ioScheduler: Scheduler
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val repoNames: LiveData<List<GithubRepo>> by lazy {
        LiveDataReactiveStreams.fromPublisher(
            repository.get()
                .subscribeOn(ioScheduler)
                .toFlowable(BackpressureStrategy.LATEST)
                .onErrorReturn {
                    Timber.e(it)
                    emptyList()
                }
        )
    }

    fun refresh() {
        repository.getFromRemote()
            .subscribeOn(ioScheduler)
            .subscribeBy(
                onError = { it.printStackTrace() }
            ).addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}