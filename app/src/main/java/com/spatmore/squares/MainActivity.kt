package com.spatmore.squares

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.spatmore.core.common.MarginItemDecoration
import com.spatmore.core.common.MarginItemDecoration.Orientation.HORIZONTAL
import com.spatmore.core.common.MarginItemDecoration.Orientation.VERTICAL
import com.spatmore.core.ext.observeNonNull
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val repoAdapter = RepoAdapter()

        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = repoAdapter
            layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.list_top_edge_slide_in)
            addItemDecoration(
                MarginItemDecoration(
                    resources.getInteger(R.integer.recycler_item_margin),
                    VERTICAL,
                    HORIZONTAL
                )
            )
        }

        viewModel.repoNames.observeNonNull(this, Observer { detailsList ->
            swipe_refresh_view.isRefreshing = false

            if (repoAdapter.models != detailsList) {
                repoAdapter.models = detailsList
                recycler_view.scheduleLayoutAnimation()
            }
        })

        swipe_refresh_view.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    companion object {

        fun getIntent(context: Context) = Intent(context, MainActivity::class.java)
    }
}